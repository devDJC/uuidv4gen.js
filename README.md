# uuidv4gen.js

A UUID/GUID generator based on RFC 4122 written in JavaScript

Add script via html script tag 
Example:

`<script src="script/uuidv4gen.js" defer></script>`