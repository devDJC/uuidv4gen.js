/*
uuidv4gen.js created by: David Clarke 09/09/2019
Generates a UUID v4 based on random number generation

1. The program uses an javascript array to store each byte value in string(output) form

2. Bytes are processed by 4bit nibble(half-a-byte) based on a random Math.random(15) 0-15 number

3. Bytes are concatonated between strings and number nibbles(converted to strings) 

4. The final byte is stored in output array

5. Two parts have special processing a reserved space and version number


based upon rfc4122
https://www.ietf.org/rfc/rfc4122.txt

60bit max number for timestamp
2 pow 60 
1152921504606846975

14 bit for clock sequence
2 pow 14
16384

node field 48
2 pow 48
281474976710656

 Algorithms for Creating a UUID from Truly Random or
      Pseudo-Random Numbers

   The version 4 UUID is meant for generating UUIDs from truly-random or
   pseudo-random numbers.

   The algorithm is as follows:

   o  Set the two most significant bits (bits 6 and 7) of the
      clock_seq_hi_and_reserved to zero and one, respectively.

   o  Set the four most significant bits (bits 12 through 15) of the
      time_hi_and_version field to the 4-bit version number from
      Section 4.1.3.

   o  Set all the other bits to randomly (or pseudo-randomly) chosen
      values.


      4.1.2.  Layout and Byte Order

   To minimize confusion about bit assignments within octets, the UUID
   record definition is defined only in terms of fields that are
   integral numbers of octets.  The fields are presented with the most
   significant one first.

   Field                  Data Type     Octet  Note
                                        #

   time_low               unsigned 32   0-3    The low field of the
                          bit integer          timestamp

   time_mid               unsigned 16   4-5    The middle field of the
                          bit integer          timestamp

   time_hi_and_version    unsigned 16   6-7    The high field of the
                          bit integer          timestamp multiplexed

   clock_seq_hi_and_rese  unsigned 8    8      The high field of the
   rved                   bit integer          clock sequence
                                               multiplexed with the
                                               variant

   clock_seq_low          unsigned 8    9      The low field of the
                          bit integer          clock sequence

   node                   unsigned 48   10-15  The spatially unique
                          bit integer          node identifier
*/



//returns a pseudo-random generated UUID
function generateUUID(){
    
    let hexCode = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'];
    let uuidcounter = 0;
    let UUID = new Array(16+4);
    let nibble = 0; //0-15
    let hexbyte = "";

    //genrate bytes.
    //javascript does bitwise on 32 bit numbers but stores numbers on 64 bits.active
    //so technically, I should store a random 60 bit number and not use bitwise at all. MSB on 64 bit used for negative flag
    
    //not used
    //let genbits = Math.floor(Math.random() * 1152921504606846975); //generates random number from 0 to the largest number the bits can hold
    //let clockseq = Math.floor(Math.random() * 16384);
    //let node = Math.floor(Math.random() * 281474976710656);


    //only store bytes //store the hex?
    //16 hex values and 4 dashes
    //string representation
    

    //process time-low 4bytes
   for(let i = 1; i < 9; i++){
        nibble = hexCode[Math.floor(Math.random() * 15)]; //0-15
        hexbyte = hexbyte + nibble;
        //console.log("nib" + i + " " + nibble);

        if((i % 2) == 0){
            UUID[uuidcounter] = hexbyte;
            hexbyte = '';
            uuidcounter++;
        }
    }
    //console.log("UUID 1 : " + UUID);
    //uuidcounter++;
    //$UUID[0] = $hexCode[byte0];//0123

    UUID[uuidcounter] = '-';
    uuidcounter++;

    //process time-mid 2 bytes
    hexbyte = "";
    for(let i = 1; i < 5; i++){
        nibble = hexCode[Math.floor(Math.random() * 15)]; //0-15
        hexbyte = hexbyte + nibble;
        //console.log("nib" + i + " " + nibble);

        if((i % 2) == 0){
            UUID[uuidcounter] = hexbyte;
            hexbyte = '';
            uuidcounter++;
        }
   }

   //console.log(UUID);
   
   //uuidcounter++;
   UUID[uuidcounter] = '-';
   uuidcounter++;

   //process time-high-and-version 2bytes
   //Set the four most significant bits (bits 12 through 15) of the time_hi_and_version field to the 4-bit version number from Section 4.1.3.
   //set 4 most significant bits 0100 or 4
   let version = '4';
   nibble = hexCode[Math.floor(Math.random() * 15)]; //0-15
   hexbyte = version + nibble;

   UUID[uuidcounter] = hexbyte;
   uuidcounter++;

   nibble = hexCode[Math.floor(Math.random() * 15)]; //0-15
   hexbyte = nibble.toString();
   nibble = hexCode[Math.floor(Math.random() * 15)]; //0-15
   hexbyte = hexbyte + nibble;
   UUID[uuidcounter] = hexbyte;
   uuidcounter++;

   UUID[uuidcounter] = "-";
   uuidcounter++;

   //console.log("UUDI 2 = " + UUID);

   //process clock-seq-and-reserved 1byte
   //Set the two most significant bits (bits 6 and 7) of the clock_seq_hi_and_reserved to zero and one, respectively.
   //sets two significant bits to 1 and 0, so 8 to B depending on nibble?
   let reserved = "8";
   let halfnibble = Math.floor(Math.random() * 3);
   if(halfnibble === 1){
       reserved = "9";
   }

   if(halfnibble === 2){
        reserved = "a";
   }

   if(halfnibble === 3){
        reserved = "b";
   }

   nibble = hexCode[Math.floor(Math.random() * 15)];
   hexbyte = reserved + nibble;

   UUID[uuidcounter] = hexbyte;
   uuidcounter++;
   
   //process clock-seq-low 1byte

   nibble = hexCode[Math.floor(Math.random() * 15)];
   hexbyte = nibble;
   nibble = hexCode[Math.floor(Math.random() * 15)];
   hexbyte = hexbyte + nibble;

   UUID[uuidcounter] = hexbyte;
   uuidcounter++;

   UUID[uuidcounter] = '-';
   uuidcounter++;

   //console.log ("UUID 3: " + UUID);


   //process node section process 6bytes
   //Set all the other bits to randomly (or pseudo-randomly) chosen values.
   hexbyte = "";
   for(let i = 1; i < 13; i++){
    nibble = hexCode[Math.floor(Math.random() * 15)]; //0-15
    hexbyte = hexbyte + nibble;

    if((i % 2) == 0){
        UUID[uuidcounter] = hexbyte;
        hexbyte = '';
        uuidcounter++;
    }

}

//console.log ("UUID 4: " + UUID);


return UUID.join('');
   

}